#crtcmod

Create c module. A borgi utility.

```
$ crtcmod -h
Syntax:
  crtcmod
    -h|--help
    -r|--root /rootpath/mychroot (1)
    -s|--src mycool.c
    -l|--lib mylib (*LIBL) (2)
    -i|--include /project/include (3)
    -g|--debug
    -o|--option "TEXT('my module')"
    -v|--verbose
Note:
 (1) -r|--root alternative
     > export CHROOT=/rootpath/mychroot
 (2) -l|--lib mylib alternative
     > export LIBL="MYLIB YOURLIB BOBLIB"
     > export ADDLIB=MYLIB
     > export CURLIB=MYLIB
 (3) -i|--include
     chroot relative (do not add CHROOT)
     default is current directory (pwd)
```

#NOTE

**CHROOT or -r|--root** - /rootpath/mychroot is 
/rootpath subdir starting location of your chroot.
This must be included to find objects in your chroot.

**module.c > module.mod** - enable Makefile processing with timestamps 
crtcmod creates a module.mod in current directory 
on successful compile. This additional empty module.mod file allows 
normal Makefile processing to only compile on source module.c changes.

Example (inside chroot /QOpenSys/db2sock):
```
bash-4.3$ crtcmod -s db2json.c -l db2json -v
borgi -cmd "CRTCMOD MODULE(db2json/db2json) REPLACE(*YES) TGTCCSID(37) SRCSTMF('/QOpenSys/db2sock/home/db2sock/db2sock/ILE-CGI/db2json.c') INCDIR('/QOpenSys/db2sock/home/db2sock/db2sock/ILE-CGI') OUTPUT(*NONE)"
-success db2json
```
