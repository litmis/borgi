#dltlib

Delete library. A borgi utility.

```
$ dltlib -h
Syntax:
  dltlib
    -h|--help
    -r|--root /rootpath/mychroot (1)
    -l|--lib mylib
    -v|--verbose
    -o|--option "ASPDEV(BOB)"
Note:
 (1) -r|--root alternative
     > export CHROOT=/rootpath/mychroot
```

#NOTE

**CHROOT or -r|--root** - /rootpath/mychroot is 
/rootpath subdir starting location of your chroot.
This must be included to find objects in your chroot.

Example (inside chroot /QOpenSys/db2sock):
```
bash-4.3$ dltlib -l frogblow
              
bash-4.3$ dltlib -l frogblow
-msgid SQL0443 -msgtype DIAGNOSTIC -msgsub *NULL -msgsev 30 -msgstamp 2017-06-22-13.09.24.902671 -msgtolib QSYS -msgtopgm QSQRUN4 -msgtomod QSQCALLSP -msgtoproc CLEANUP -msgtoinst 42334 -msgtxt "Trigger program or external routine detected an error."
-msgid CPF2110 -msgtype ESCAPE -msgsub EXCEPTION HANDLED -msgsev 40 -msgstamp 2017-06-22-13.09.24.902468 -msgtolib QSYS -msgtopgm QSQRUN4 -msgtomod QSQCALLSP -msgtoproc CALLPROGRAM -msgtoinst 43225 -msgtxt "Library FROGBLOW not found."

```
