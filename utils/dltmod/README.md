#dltmod

Delete module. A borgi utility.

```
$ dltmod -h
Syntax:
  dltmod
    -h|--help
    -r|--root /rootpath/mychroot (1)
    -l|--lib mylib (*LIBL) (2)
    -m|--mod mycool (3)
    -v|--verbose
Note:
 (1) -r|--root alternative
     > export CHROOT=/rootpath/mychroot
 (2) -l|--lib mylib alternative
     > export LIBL="MYLIB YOURLIB BOBLIB"
     > export ADDLIB=MYLIB
     > export CURLIB=MYLIB
 (3) -m|--mod mycool
     -m|--mod mycool.mod
     -m|--mod mycool.any
     Any mycool, mycool.suffix is valid.
     (Makefile may use mycool.mod)
```

#NOTE

**CHROOT or -r|--root** - /rootpath/mychroot is 
/rootpath subdir starting location of your chroot.
This must be included to find objects in your chroot.


Example (inside chroot /QOpenSys/db2sock):
```
```
