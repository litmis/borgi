#wrksyssts

Display wrksyssts. A borgi utility.

```
$ wrksyssts -h
Syntax:
  wrksyssts
    -h|--help
    -r|--root /rootpath/mychroot (1)
    -o|--option "RESET(*NO) ASTLVL(*PRV)"
Note:
 (1) -r|--root alternative
     > export CHROOT=/rootpath/mychroot
```

#NOTE

**CHROOT or -r|--root** - /rootpath/mychroot is 
/rootpath subdir starting location of your chroot.
This must be included to find objects in your chroot.

Example (inside chroot /QOpenSys/db2sock):
```
bash-4.3$ wrksyssts
                                              System Status Information                                                  Page     1
 5770SS1 V7R1M0  100423                                                                            UT28P63   06/22/17  13:51:56 CDT
  % CPU used . . . . . . . . . . . . . . :     4.9                   System ASP . . . . . . . . . . . . . . :    458.1 G
  % uncapped CPU capacity used . . . . . :     1.2                   % system ASP used  . . . . . . . . . . :    21.2715
...
```
