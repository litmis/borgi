#db2script

Run DB2 script file. A borgi utility.

```
$ db2script -h
Syntax:
  dltmod
    -h|--help
    -f|--file /path/myfile (1)
    -r|--replace haystack needle (2)
    -v|--verbose
Note:
 (1) -f|--file /path/myfile
     chroot relative (do not add CHROOT)
 (2) -r|--replace haystack needle
     example: -r XMLSERVICE MYLIB -r PLUG WIREE
```

Example (inside chroot /QOpenSys/db2sock):
```

bash-4.3$ head  ../../xmlservice-rpg/crtsql.cmd        
DROP PROCEDURE XMLSERVICE.iPLUG4K;
DROP PROCEDURE XMLSERVICE.iPLUG32K;
DROP PROCEDURE XMLSERVICE.iPLUG65K;
...


bash-4.3$ db2script -f  ../../xmlservice-rpg/crtsql.cmd
borgi -db2 " DROP PROCEDURE XMLSERVICE.iPLUG4K"
borgi -db2 " DROP PROCEDURE XMLSERVICE.iPLUG32K"
borgi -db2 " DROP PROCEDURE XMLSERVICE.iPLUG65K"
...

bash-4.3$ db2script -f  ../../xmlservice-rpg/crtsql.cmd -r XMLSERVICE FROG
borgi -db2 " DROP PROCEDURE FROG.iPLUG4K"
Error IPLUG4K in FROG type *N not found. SQLCODE=-204
borgi -db2 " DROP PROCEDURE FROG.iPLUG32K"
Error IPLUG32K in FROG type *N not found. SQLCODE=-204
borgi -db2 " DROP PROCEDURE FROG.iPLUG65K"
Error IPLUG65K in FROG type *N not found. SQLCODE=-204

```
