#dsplibl

Display library. A borgi utility.

```
$ dsplib -h
Syntax:
  dsplib
    -h|--help
    -l|--lib mylib
    -r|--root /rootpath/mychroot (1)
Note:
 (1) -r|--root alternative
     > export CHROOT=/rootpath/mychroot
```

#NOTE

**CHROOT or -r|--root** - /rootpath/mychroot is 
/rootpath subdir starting location of your chroot.
This must be included to find objects in your chroot.

Example (inside chroot /QOpenSys/db2sock):
```
bash-4.3$ dsplib -l xmlservice
  5770SS1 V7R1M0  100423                   Display Library                                        6/20/17 12:39:51        Page    1
  Library . . . . . . . . . . . . . . . . :   XMLSERVICE
  Type  . . . . . . . . . . . . . . . . . :   PROD
  Number of objects . . . . . . . . . . . :           30
  Library ASP number  . . . . . . . . . . :       1
  Library ASP device  . . . . . . . . . . :   *SYSBAS
  Library ASP group   . . . . . . . . . . :   *SYSBAS
  Create authority  . . . . . . . . . . . :   *SYSVAL
  Text description  . . . . . . . . . . . :   Open Source i
    Object      Type      Attribute             Size  Description
    CRTTEST     *PGM      CLP                  57344
    CRTTEST6    *PGM      CLP                  45056
    CRTXML      *PGM      CLP                  65536
...
```
