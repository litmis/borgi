#dsplibl

Display library list. A borgi utility.

```
$ dsplibl -h
Syntax:
  dsplibl
    -h|--help
    -r|--root /rootpath/mychroot (1)
Note:
 (1) -r|--root alternative
     > export CHROOT=/rootpath/mychroot
```

#NOTE

**CHROOT or -r|--root** - /rootpath/mychroot is 
/rootpath subdir starting location of your chroot.
This must be included to find objects in your chroot.

Example (inside chroot /QOpenSys/db2sock):
```
bash-4.3$ dsplibl   
borgi -qsh 'DSPLIBL OUTPUT(*PRINT) ' -r /QOpenSys/db2sock
  5770SS1 V7R1M0  100423                    Library List                                          6/20/17 12:19:41        Page    1
                           ASP
    Library     Type       Device      Text Description
    QSYS        SYS                    System Library
    QSYS2       SYS                    System Library for CPI's
    QHLPSYS     SYS
    QUSRSYS     SYS                    System Library for Users
    QSHELL      PRD
    QGPL        USR                    General Purpose Library
    QTEMP       USR
    QDEVELOP    USR
    QBLDSYS     USR
    QBLDSYSR    USR
                           * * * * *  E N D  O F  L I S T I N G  * * * * *
```
