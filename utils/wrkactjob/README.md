#wrkactjob

Display wrkactjob. A borgi utility.

```
$ wrkactjob -h
Syntax:
  wrkactjob
    -h|--help
    -r|--root /rootpath/mychroot (1)
    -s|--sbs subsystem
    -j|--job jobname*
    -o|--option "CPUPCTLMT(20)"
Note:
 (1) -r|--root alternative
     > export CHROOT=/rootpath/mychroot
```

#NOTE

**CHROOT or -r|--root** - /rootpath/mychroot is 
/rootpath subdir starting location of your chroot.
This must be included to find objects in your chroot.

Example (inside chroot /QOpenSys/db2sock):
```
bash-4.3$ wrkactjob -s QUSRWRK -j QZD*                                                  
borgi -qsh 'WRKACTJOB OUTPUT(*PRINT) SBS(QUSRWRK) JOB(QZD*)' -r /QOpenSys/db2sock
  5770SS1 V7R1M0 100423                  Work with Active Jobs                                    6/20/17 11:58:15        Page    1
  Reset . . . . . . . . . . . . . . . . . :   *NO
  Subsystems  . . . . . . . . . . . . . . :   QUSRWRK
  CPU Percent Limit . . . . . . . . . . . :   *NONE
  Response Time Limit . . . . . . . . . . :   *NONE
  Sequence  . . . . . . . . . . . . . . . :   *SBS
  Job name  . . . . . . . . . . . . . . . :   QZD*
  CPU %  . . . :      .0          Elapsed time . . . . . . . :   00:00:00           Active jobs . . . . . . :    238
                                      Current                             --------Elapsed---------
  Subsystem/Job  User        Number   User        Type Pool Pty     CPU   Int    Rsp  AuxIO   CPU%  Function       Status   Threads
    QZDASOINIT   QUSER       091687   QSECOFR     PJ     2  20        .0                  0     .0                  TIMW          1
    QZDASOINIT   QUSER       092077   QUSER       PJ     2  20        .3                  0     .0                  PSRW          1
    QZDASOINIT   QUSER       267586   QUSER       PJ     2  20        .0                  0     .0                  PSRW          1
    QZDASSINIT   QUSER       091689   QUSER       PJ     2  20        .0                  0     .0                  PSRW          1
                           * * * * *  E N D  O F  L I S T I N G  * * * * *
```
