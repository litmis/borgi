#dltf

Delete file. A borgi utility.

```
$ dltf -h
Syntax:
  dltf
    -h|--help
    -r|--root /rootpath/mychroot (1)
    -l|--lib mylib (*LIBL) (2)
    -f|--file qclsrc
    -v|--verbose
    -o|--option "SYSTEM(*LCL) RMVCST(*RESTRICT)"
Note:
 (1) -r|--root alternative
     > export CHROOT=/rootpath/mychroot
 (2) -l|--lib mylib alternative
     > export LIBL="MYLIB YOURLIB BOBLIB"
     > export ADDLIB=MYLIB
     > export CURLIB=MYLIB
```

#NOTE

**CHROOT or -r|--root** - /rootpath/mychroot is 
/rootpath subdir starting location of your chroot.
This must be included to find objects in your chroot.

Example (inside chroot /QOpenSys/db2sock):
```
bash-4.3$ dltf -f tonysrc -l xmlservice    
```
