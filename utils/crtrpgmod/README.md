#crtrpgmod

Create rpg module. A borgi utility.

```
$ crtrpgmod -h
Syntax:
  crtrpgmod
    -h|--help
    -r|--root /rootpath/mychroot (1)
    -s|--src mycool.rpgle
    -l|--lib mylib (*LIBL) (2)
    -i|--include /project/include (3)
    -g|--debug
    -o|--option "TEXT('my module')"
    -v|--verbose
Note:
 (1) -r|--root alternative
     > export CHROOT=/rootpath/mychroot
 (2) -l|--lib mylib alternative
     > export LIBL="MYLIB YOURLIB BOBLIB"
     > export ADDLIB=MYLIB
     > export CURLIB=MYLIB
 (3) -i|--include
     chroot relative (do not add CHROOT)
     default is current directory (pwd)
```

#NOTE

**CHROOT or -r|--root** - /rootpath/mychroot is 
/rootpath subdir starting location of your chroot.
This must be included to find objects in your chroot.

**module.rpgle > module.mod** - enable Makefile processing with timestamps 
crtrpgmod creates a module.mod in current directory 
on successful compile. This additional empty module.mod file allows 
normal Makefile processing to only compile on source module.rpgle changes.

Example (inside chroot /QOpenSys/db2sock):
```
bash-4.3$ cd ../xmlservice-rpg/
bash-4.3$ crtrpgmod -s plugerr.rpgle -l xmlservice -o "text('tony was here')"    
borgi -qsh 'CRTRPGMOD MODULE(xmlservice/plugerr) REPLACE(*YES) TGTCCSID(37) 
  SRCSTMF('/QOpenSys/db2sock/home/db2sock/xmlservice-rpg/plugerr.rpgle') 
  INCDIR('/QOpenSys/db2sock/home/db2sock/xmlservice-rpg')  text('tony was here')'
  -r /QOpenSys/db2sock
==> xmlservice/plugerr -- 00 highest severity
```
