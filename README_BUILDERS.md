#borgi

Welcome to the borgi project. Goal is chroot safe 'system' utility.

# UTILITY BUILDERS
Use any of the existing utilities as pattern to add your own.
Then add you utility to cpyutil2bin.sh 'utils' for installation on user machines.
```
===
copy current utility
===
$ cd borgi/utils
$ mkdir dltlib
$ cp crtlib/* dltlib/.
$ cd dltlib/
$ ls
crtlib  README.md
$ mv crtlib dltlib

===
add to cpyutil2bin.sh (utils)
===
#!/bin/sh
TARGET=/QOpenSys/usr/bin
utils="crtpgm crtsrvpgm crtrpgmod dltlib ...
for u in $utils
do
  echo "chmod +x $u/$u"
  chmod +x "$u/$u"
  echo "cp $u/$u $TARGET/$u"
  cp "$u/$u" "$TARGET/$u"
done
```

# Building borgi from source 

#Pre-compile
```
copy ILE headers to PASE
$ ./cpysqlincludes.sh

copy ILE style sqlcli1.h
$ cp pase_includes/sqlcli1.h /QOpenSys/usr/include
```

#Compile

I am using a chroot with following packages from [ibmichroot](https://bitbucket.org/litmis/ibmichroot). 
```
> pkg_setup.sh pkg_perzl_gcc-4.8.3.lst
```
I needed a new gmake, you can find these on [yips Open Source](http://yips.idevcloud.com/wiki/index.php/PASE/OpenSourceBinaries)
```
Binary fixes (GCC OPS):
    Attach:gmake-4.2.zip — newer version
    Attach:m4-1.4.17.zip — newer version
        copy to directory /opt/freeware/bin 
```

Make binary borgi

```
$ export PATH=/opt/freeware/bin:$PATH
$ export LIBPATH=/opt/freeware/lib:$LIBPATH
$ cd borgi
$ gmake -f Makefile
```

#Note

This technology uses DB2 server mode to escape chroot and run IBM i commands in 'root' of the machine. 
DB2 QSQSRVR jobs are pre-started on any machine long before you entered chroot, therefore a valid chroot escape. 
To wit, borgi is a IBM i command IPC by QSQSRVR. 

```
Example (inside chroot /QOpenSys/db2sock):
$ borgi "WRKACTJOB" /QOpenSys/db2sock
```

The borgi second parameter /rootpath/chroot allows qsh command running outside chroot to find your chroot /tmp (again, QSQSRVR job).
After QSQSRVR job runs the qsh command ("WRKACTJOB"), /tmp/output760278.txt is read by borgi and written to stdout.
Thereby borgi acts like any other script utility for stdout pipes.


```
Example (inside chroot /QOpenSys/db2sock):
$ borgi "WRKACTJOB" /QOpenSys/db2sock

inside chroot (/QOpenSys/db2sock):
bash-4.3$ ls /tmp/output760278.txt
/tmp/output760278.txt

outside chroot (/QOpenSys/db2sock):
bash-4.3$ ls /QOpenSys/db2sock/tmp/output760278.txt
/QOpenSys/db2sock/tmp/output760278.txt
```

Note: The /tmp/output760278.txt file is deleted after write to stdout.


# XMLSERVICE (rare use)
Mostly, borgi handles all operations. However, needing complex
calls, aka, rtvjoba cmd i/o params, call my own RPG program, etc.,
you can use XMLSERVICE.

Using db2 option also works for XMLSERVICE result set option lib.iplugr512k(?,?,?).
This allows creation of complex utilities like rtvjoba, or calling a user program
to be included in scripting via borgi.

```
===
call qxmlserv.iplugr512k(?,?,?)
? - ipc - /path/route private connection (xtoolkit job)
? - ctl - *here (no ipc) or *sbmjob (ipc)
? - xml - xml request (all one line)
Note:
    qxmlserv - Apache DG0 product PTF (use any compiled xmlservice lib, ZENDSVR6, XMLSERVICE, etc.)
    xmlservice - ZZVLAD test download xmlservice yips (call crttest)
    xml input (-?) and output (-OUT151) below all one line for shell scripting (line feed visual only)
===
borgi -d "call qxmlserv.iplugr512k(?,?,?)" -? "na" "*here" 
"<?xml version='1.0'?><script><cmd exec='rexx'>RTVJOBA CCSID(?N) USRLIBL(?)</cmd></script>"
 -OUT151 
"<?xml version='1.0'?>
<script>
<cmd exec='rexx'><success>+++ success RTVJOBA CCSID(?N) USRLIBL(?)</success> 
<row> <data desc='CCSID'>37</data> </row> 
<row> <data desc='USRLIBL'>QGPL       QTEMP      QDEVELOP   QBLDSYS    QBLDSYSR</data> </row> 
</cmd> 
</script>"



bash-4.3$ borgi -d "call qxmlserv.iplugr512k(?,?,?)" -? "na" "*here" 
"<?xml version='1.0'?>
<script>
<pgm name='ZZVLAD' lib='XMLSERVICE'>
 <parm  io='both'>
   <ds dim='3' var='job_t'>
    <data type='10a' var='begin'>na</data>
    <data type='10a' var='end'>na</data>
    <data type='64a' varying='on' var='job'>na</data>
    <data type='12p2' var='salary'>0.0</data>
   </ds>
 </parm>
</pgm>
</script>"
 -OUT151 
<parm  io='both'>
<ds dim='3' var='job_t'>
<data type='10a' var='begin'>2011-05-11</data>
<data type='10a' var='end'>2011-07-12</data>
<data type='64a' varying='on' var='job'>Frog wrangler</data>
<data type='12p2' var='salary'>7.25</data>
</ds>
<ds dim='3' var='job_t'>
<data type='10a' var='begin'>2010-01-11</data>
<data type='10a' var='end'>2010-07-12</data>
<data type='64a' varying='on' var='job'>Toad wrangler</data>
<data type='12p2' var='salary'>4.29</data>
</ds>
<ds dim='3' var='job_t'>
<data type='10a' var='begin'>2009-05-11</data>
<data type='10a' var='end'>2009-07-12</data>
<data type='64a' varying='on' var='job'>Lizard wrangler</data>
<data type='12p2' var='salary'>1.22</data>
</ds>
</parm>
<success><![CDATA[+++ success XMLSERVICE ZZVLAD ]]></success>
</pgm>
</script>"
```


