### gmake options (see make_libdb400.sh)
### - gmake -f Makefile
### gcc options (Makefile)
### -v            - verbose compile
### -Wl,-bnoquiet - verbose linker
### -shared       - shared object
### -maix64       - 64bit
### -isystem      - compile PASE system headers
### -nostdlib     - remove libgcc_s.a and crtcxa_s.o
CC          = gcc
# CCFLAGS32   = -v verbose
CCFLAGS32   = -g -Wno-int-to-pointer-cast -Wno-pointer-to-int-cast
CCFLAGS64   = $(CCFLAGS32) -maix64 -DTGT64
AR          = ar
AROPT       = -X32_64
INCLUDEPATH = -isystem /QOpenSys/usr/include -I.

### borgi
LIBDEP400       = -L. -ldb400 -L/QOpenSys/usr/lib -nostdlib -lpthreads -lc -liconv -ldl -lpthread
borgi32         = borgi
borgiLIBOBJS32  = borgi.o
borgiLIBDEPS32  = /QOpenSys/usr/lib/crt0.o $(LIBDEP400)

### global
CCFLAGS      = $(CCFLAGS32)
borgi        = $(borgi32)
borgiLIBOBJS = $(borgiLIBOBJS32)
borgiLIBDEPS = $(borgiLIBDEPS32)

### tells make all things to do (ordered)
all: clean removeo $(borgi) install

### PASE
### generic rules
### (note: .c.o compiles all c parts in OBJS list)
.SUFFIXES: .o .c
.c.o:
	$(CC) $(CCFLAGS) $(INCLUDEPATH) -c $<

### -- borgi
$(borgi32): $(borgiLIBOBJS)
	$(CC) $(CCFLAGS) $(borgiLIBOBJS) $(borgiLIBDEPS) -o $(borgi)

### -- oh housekeeping?
clean:
	rm -f $(borgi)
removeo:
	rm -f *.o
install:
	cp ./borgi /QOpenSys/usr/bin/.

