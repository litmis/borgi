![borgi.png](https://bitbucket.org/repo/7EzaEKE/images/3407629662-borgi.png)
# Borgi
Welcome to borgi project. Goal is chroot  friendly 'system' utility.

The project parts:

1) borgi - chroot friendly PASE utility (binary Yips). You do not need borgi/utils to use borgi.

2) borgi/utils (optional) - chroot friendly IBM i shell script commands using borgi (wrkactjob, crtrpgmod, crtpgm, etc.)

PASE borgi alternative was created to address many limitations of PASE 'system' in chroot (missing OUTPUT).
PASE borgi expands many common tasks beyond PASE 'system', including, db2 (strsql-like), commands without OUTPUT (qcmd), etc.
PASE borgi is designed scripting and Makefile friendly.
PASE borgi additional utilities are optional, intended useful in Makefile and/or ssh sessions.

##Why Open Source borgi?

This project is Open Source to provide a community expandable tool (see source/utils).

Second purpose is PASE c programming education. PASE borgi demostrates powerful function
with very few lines of c code (see source/borgi.c). The optional utility scripts are powerful,
simple to understand, useful shell scripting education (see source/borgi/utils). 


#Compiled version borgi

You may try pre-compiled test version borgi at Yips link (V7r1+).

* http://yips.idevcloud.com/wiki/index.php/Databases/Borgi

```
install:
/QOpenSys/usr/bin/borgi
```

#Run

The easy way to run borgi and utilities in chroot is set env var CHROOT.
Where CHROOT is path to reach your chroot subdir. 
Both borgi and utilities support env var CHROOT, to avoid -r|--root.

```
$ export CHROOT=/path/mychroot
```

Your profile provides default LIBL, 
any borgi operations use LIBL as appropriate.
However, easy way to run borgi with alternative LIBL follows. 
Both borgi and utilities support LIBL to avoid -l|--lib.
You may set any, all, or none of the following env vars.
If any env var set, borgi runs 
1-CHGLIBL, 2-ADDLIBLE, 3-CHGCURLIB (ordered),
before excuting the operation (-qsh, -db2, -cmd).
```
$ export LIBL="MYLIB YOURLIB BOBLIB"
$ export ADDLIB=MYLIB
$ export CURLIB=MYLIB
```
Note:  


```
===
help
===
$ borgi -h
Syntax (Version 1.1.2):
  borgi -q|-qsh -c|-cmd -d|-db2 command/sql [-root /path/mychroot | -? db2parm1 'db2 parm 2' ...]
    -q|-qsh    - qsh system '*CMD' with *OUTPUT expected
    -c|-cmd    - *CMD no *OUTPUT expected
    -d|-db2    - DB2 SQL
    -r|-root   - chroot root /path/mychroot (CHROOT)
                 $export CHROOT=/path/mychroot
   -ll|-libl   - *LIBL CHGLIBL before command execution (LIBL)
                 $export LIBL="MYLIB YOURLIB BOBLIB"
   -al|-addlib - *LIBL ADDLIBLE before command execution (ADDLIB)
                 $export ADDLIB=MYLIB
   -cl|-curlib - *LIBL CHGCURLIB before command execution (CURLIB)
                 $export CURLIB=MYLIB
===
qsh system 'cmd'
===
Example (no chroot):
$ borgi -q "WRKACTJOB"

Example (inside chroot /QOpenSys/db2sock):
$ borgi -q "WRKACTJOB" -r /QOpenSys/db2sock

===
"cmd"
===
bash-4.3$ borgi -c "ADDLIBLE LIB(FROGNOT)"
-msgid SQL0443 -msgtype DIAGNOSTIC -msgsub *NULL -msgsev 30 -msgstamp 2017-06-20-13.24.27.168495 -msgtolib QSYS -msgtopgm QSQRUN4 -msgtomod QSQCALLSP -msgtoproc CLEANUP -msgtoinst 42334 -msgtxt "Trigger program or external routine detected an error."
-msgid CPF2110 -msgtype ESCAPE -msgsub EXCEPTION HANDLED -msgsev 40 -msgstamp 2017-06-20-13.24.27.168228 -msgtolib QSYS -msgtopgm QSQRUN4 -msgtomod QSQCALLSP -msgtoproc CALLPROGRAM -msgtoinst 43225 -msgtxt "Library FROGNOT not found."

===
"db2"
===
$ borgi -d "select LSTNAM,CITY,STREET,ZIPCOD,BALDUE from QIWS/QCUSTCDT where LSTNAM=? or LSTNAM=?" -? Jones Vine
 -LSTNAM Jones -CITY Clay -STREET "21B NW 135 St" -ZIPCOD 13041 -BALDUE 100.00
 -LSTNAM Vine -CITY Broton -STREET "PO Box 79" -ZIPCOD 5046 -BALDUE 439.00

```




#Utilities (optional)

Various chroot friendly utilities are available with borgi.
See project source/utils/command/README.md for details each IBM i utility.

##Operation

* wrkactjob - IBM i wrkactjob.
* wrksyssts - IBM i wrksysts.
* rtvjoba - IBM i command rtvjoba.

##Library

* dsplibl - IBM i dsplibl.
* dsplib - IBM i dsplib.
* crtlib - IBM i create library.
* dltlib - IBM i delete library.

##File

* crtsrcpf - IBM i create source file.
* cpyfrmstmf - IBM i from stream file.
* dltf - IBM i delete file.

##Program

* crtrpgmod - IBM i create rpg module.
* crtcmod - IBM i create c module.
* crtsqlrpgi - IBM i create rpg module with sql pre-compile.
* crtpgm - IBM i create program.
* crtsrvpgm - IBM i create service program.
* dltmod - IBM i delete module.
* crtclpgm - IBM i create CL program.

##DB2

* db2script - IBM i db2 script file run.

##Download

You may try pre-zip test version at Yips link (V7r1+).

* http://yips.idevcloud.com/wiki/index.php/Databases/Borgi
```
> cd borgi/utils
> ./cpyutil2bin.sh
```


##Run utility

```
$ export CHROOT=/QOpenSys/db2sock

$ wrkactjob -s QUSRWRK -j QZD* 
  5770SS1 V7R1M0 100423                  Work with Active Jobs ...  

$ dsplibl   
  5770SS1 V7R1M0  100423                    Library List ...                                              

$ dsplib -l xmlservice
  5770SS1 V7R1M0  100423                   Display Library ...

$ rtvjoba CURUSER CCSID USRLIBL 
-CURUSER DB2SOCK -CCSID 37 -USRLIBL "QGPL QTEMP QDEVELOP QBLDSYS QBLDSYSR"

$ crtrpgmod -s xmlmain.rpgle -l xmlservice

$ crtsqlrpgi -s xmlstoredp.rpglesql -l xmlservice -o "text('tony was here')"

$ crtpgm -p xmlmain -l xmlservice -m xmlmain plugbug plugipc plugrun ...

```

# BUILDERS

Information for building borgi and utilities (see source/README_BUILDERS.md).

#Contributors
- Tony Cairns, IBM
- Aaron Bartell, Krengel Technology, Inc.

#License
MIT

